# django-docker

## Define the project components (https://docs.docker.com/compose/django/)

For this project, you need to create a Dockerfile, a Python dependencies file, and a docker-compose.yml 

1. Create an empty project directory.

You can name the directory something easy for you to remember. This directory is the context for your application image. The directory should only contain resources to build that image.

2. Create a new file called Dockerfile in your project directory.

The Dockerfile defines an application’s image content via one or more build commands that configure that image. Once built, you can run the image in a container.

3. Add the following content to the Dockerfile.

```
FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/

```

4. Create a requrements.txt file

```
Django>=3.0,<3.1
psycopg2>=2.8,<2.9

```
psycopg2 is for postres database

5. Create a file called docker-compose.yml

```
  version: '3'
    
  services:
    db:
      image: postgres
      environment:
        - POSTGRES_DB=postgres
        - POSTGRES_USER=postgres
        - POSTGRES_PASSWORD=postgres
    web:
      build: .
      command: python manage.py runserver 0.0.0.0:8000
      volumes:
        - .:/code
      ports:
        - "8000:8000"
      depends_on:
        - db

```
6. Create the django project
```
sudo docker-compose run web django-admin startproject composeexample .
```
7. change user permissions
```
sudo chown -R $USER:$USER .
```

8. connect to the database
```
# setting.py
   
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}
```
9. test
```
docker-compose up 
```

10. migrations
```
docker-compose run web python manage.py migrate
```

11. superuser creation
```
docker-compose run web python manage.py createsuperuser
```

to check:
https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/
